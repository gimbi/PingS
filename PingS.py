# ########################################################################### #
# Programme de test de connexions commencé le 14 avril 2012                   #
#                                                                             #
# Fonctions :                                                                 #
# 1. Pings vers INTERNET.                                                     #
# 2. Pings vers mon NAS en utilisant l'adresse IP fixe.                       #
# 3. Pings vers mon serveur en utilisant l'adresse DNS (yoam.homelinux.com).  #
# 4. Redirection de toutes ces informations dans le fichier texte pings.log   #
# ########################################################################### #

# Importation des modules necéssaire.
import os
import time
from socket import *

# Fonction pour la création et l'écriture du fichiers log.
def FicI():
    LogI = open('E:\\pings.log', 'w')
    LogI.write('Création du log\n')
    LogI.write(time.strftime('Date : %d/%m/%Y\nHeure : %H h %M min et %S s\n'))
    LogI.write('\n')
    LogI.write(' ----------------------------------------------------- \n')
    LogI.write('| Test de la connexion de ma machine vers Internet... |\n')
    LogI.write(' ----------------------------------------------------- \n')

def FicS():
    LogS = open('E:\\pings.log', 'a')
    LogS.write('\n')
    LogS.write(' ------------------------------------------------------------ \n')
    LogS.write('| 1er test de la connexion de ma machine à mon NAS....       |\n')
    LogS.write(' ------------------------------------------------------------ \n')

def FicSDNS():
    LogSDNS = open('E:\\pings.log', 'a')
    LogSDNS.write('\n')
    LogSDNS.write(' ------------------------------------------------------------- \n')
    LogSDNS.write('| 2ème test de la connexion de ma machine à mon serveur...    |\n')
    LogSDNS.write(' ------------------------------------------------------------- \n')

# Fonction renvoyant le nombres de lignes qu'il y a dans le fichier log.
def countLigneLogI():
    LogI = open('E:\\pings.log', 'r')
    lecture = LogI.readlines()
    N_lignes = len(lecture)
    if N_lignes >= 13:
        print('>>> Etat de la connexion                  : Ok.')
    else:
        print('>>> Etat de la connexion                  : ERREUR.')
    LogI.close()

# Fonction renvoyant le nombres de lignes qu'il y a dans les fichiers log.
def countLigneLogS():
    LogS = open('E:\\pings.log', 'r')
    lecture = LogS.readlines()
    N_lignes = len(lecture)
    if N_lignes >= 28:
        print('>>> Etat de la connexion                  : Ok.')
    else:
        print('>>> Etat de la connexion                  : ERREUR.')
    LogS.close()

# Fonction renvoyant le nombres de lignes qu'il y a dans les fichiers log.
def countLigneLogSDNS():
    LogSDNS = open('E:\\pings.log', 'r')
    lecture = LogSDNS.readlines()
    N_lignes = len(lecture)
    if N_lignes >= 43:
        print('>>> Etat de la connexion                  : Ok.')
        os.startfile('E:\\pings.log')
    else:
        print('>>> Etat de la connexion                  : ERREUR.')
        os.startfile('E:\\pings.log')
        os.startfile('http://dyn.com/dns/')
    LogSDNS.close()
    
# ################################ #
# Programme principale.            #
# ################################ #

# Définition du titre de la fenêtre.
os.system('title Pings1.0')
# Définition de la taille de la fenêtre.
os.system('mode con cols=64 lines=40')

# Affichage du texte de confirmation pour la création et la copie des infos dans le fichier texte.
print('\n>>> Création du fichier log "pings.log"   : Ok.\n')
# Affichage du texte.
print(' ----------------------------------------------------- \n')
print('| Test de la connexion de ma machine vers Internet... |\n')
print(' ----------------------------------------------------- \n')
# Utilisation de la fonction FicI().
FicI()
# Ping de l'adresse "www.google.fr" avec redirection vers "E:\\pings.log".
os.system('ping www.google.fr >> E:\\pings.log')
# Utilisation de la fonction countLigneLogI.
countLigneLogI()
print('')
print(' ============================================================')
print('')
# Affichage du texte.
print(' ------------------------------------------------------------ \n')
print('| 1er test de la connexion de ma machine à mon NAS...        |\n')
print(' ------------------------------------------------------------ \n')
# Utilisation de la fonction FicS().
FicS()
# Ping de l'adresse IP de mon serveur avec redirection vers "E:\\pings.log".
os.system('ping 192.168.1.99 >> E:\\pings.log')
# Utilisation de la fonction countLigneLogS.
countLigneLogS()
print('')
print(' ============================================================')
print('')
# Affichage du texte.
print(' ------------------------------------------------------------- \n')
print('| 2ème test de la connexion de ma machine à mon serveur...    |\n')
print(' ------------------------------------------------------------- \n')
# Utilisation de la fonction FicSDNS().
FicSDNS()
# Ping de l'adresse DNS pour mon serveur avec redirection vers "E:\\pings.log".
os.system('ping yoam.homelinux.com >> E:\\pings.log')
# Utilisation de la fonction countLigneLogSDNS.
countLigneLogSDNS()
print(' \n============================================================')
print('\n>>> Copie des informations                : Ok.\n')

# Attendre que l'utilisateur appuie sur la touche ENTRER pour fermer le programme.
input('Appuyer sur ENTRER pour fermer le programme.')
