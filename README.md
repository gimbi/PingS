# PingS 
(réalisé le 14 avril 2012) 

Il s'agit d'un script très simple réalisé en Python (pour plus d'infos sur ce langage vous pouvez vous rendre à cette adresse :  http://monptitnuage.fr/index.php?article21/langage-de-programmation-python

Le but est juste de faire des tests de connexions en utilisant des pings et rediriger les résultats dans des fichiers de logs afin de garder une trace.

A vous ensuite de l’adapter pour que les pings fonctionnent correctement ainsi que la redirection dans le fichier de log.
